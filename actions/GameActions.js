
export function getFeaturedGames(featuredGamesData){
    return{
        type:"GET_FEATURED_GAMES",
        games: featuredGamesData
    }
}

export function getUpcomingGamesBySport(upcomingGamesBySport){
    return{
        type:"GET_UPCOMING_GAMES_BY_SPORT",
        games: upcomingGamesBySport
    }
}

export function getUpcomingGamesBySportInProgress(){
    return{
        type:"GET_UPCOMING_GAMES_BY_SPORT_IN_PROGRESS"
    }
}