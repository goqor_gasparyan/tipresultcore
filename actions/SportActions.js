
export function getSports(sportsData){
    return{
        type:"GET_SPORTS",
        sports:sportsData
    }
}

export function selectedSportChanged(id){
    return{
        type:"SELECTED_SPORT_CHANGED",
        selected_sport:id
    }
}
