import axios from "axios";
import { getFeaturedGames } from "../actions/GameActions.js";
import { getUpcomingGamesBySport,getUpcomingGamesBySportInProgress } from "../actions/GameActions.js";

export function getFeaturedGamesData(){
     return (dispatch)=>{
        return axios.post("http://tipresult.hook.am/public/api/v1/featured-games").then((response)=>{
            dispatch(getFeaturedGames(response.data.data));
        })
    }
}

export function getUpcomingGamesBySportData(id){
     return (dispatch)=>{
         dispatch(getUpcomingGamesBySportInProgress());
        return axios.post("http://tipresult.hook.am/public/api/v1/upcoming-games",{
            sport_id: id
         }).then((response)=>{
            dispatch(getUpcomingGamesBySport(response.data.data));
        })
    }
}