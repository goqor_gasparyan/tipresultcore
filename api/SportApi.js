import axios from "axios";
import {getSports} from "../actions/SportActions.js";

export function getSportsData(){
    let sportResponse = [{'name': 'Football', 'imgPath': './../assets/images/soccer-ball-icon.png' }, {'name': 'Football', 'imgPath': './../assets/images/soccer-ball-icon.png' }];

     return (dispatch)=>{
        return axios.post("http://tipresult.hook.am/public/api/v1/sports").then((response)=>{
            dispatch(getSports(response.data.data));
        })
    }
}

export default getSportsData;