var sportResponse = [{'name': 'Football', 'imgPath': './../assets/images/soccer-ball-icon.png' }, {'name': 'Football', 'imgPath': './../assets/images/soccer-ball-icon.png' }];

const sportReducer = (state = {sports: [], isLoaded:false}, action) => {
    switch(action.type) {
        case 'GET_SPORTS':
            return Object.assign({}, state, {
                sports: action.sports,
                selected_sport: action.sports[0].id,
                isLoaded: true
            });
        case 'SELECTED_SPORT_CHANGED':
            return Object.assign({}, state, {
                selected_sport: action.selected_sport,
                isLoaded: true
            })
        default:
            return state
    }
};

export default sportReducer;