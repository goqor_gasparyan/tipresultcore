const gameReducer = (state = {games: []}, action) => {
    switch(action.type) {
        case 'GET_FEATURED_GAMES':
            return Object.assign({}, state, {
                games: action.games
            });
        case 'GET_UPCOMING_GAMES_BY_SPORT':
            return Object.assign({}, state, {
                upgames: action.games,
                isLoadedGames: true
            });

        case 'GET_UPCOMING_GAMES_BY_SPORT_IN_PROGRESS':
            return Object.assign({}, state, {
                isLoadedGames: false
            });
        default:
            return state
    }
};

export default gameReducer;